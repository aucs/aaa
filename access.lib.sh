#!/usr/bin/env bash

jar="/tmp/access-jar";
login_file="$HOME/.abdn.login";
cache="/tmp/access-cache";

mkdir -p "$cache";

curl-b(){
	curl --user-agent "Mozilla/5.0 (X11; Linux x86_64; rv:64.0) Gecko/20100101 Firefox/64.0" --silent --location --cookie "$jar" --cookie-jar "$jar" $@;
}

post(){
	curl-b $1 $2;
}

get(){
	curl-b "$1";
}

login-loop(){
	page="$1";
	printf "%s " "Logging in...";
	for i in 2 1 0; do
		sleep 0.5;
		if logged-in "$page"; then
			echo "OK";
			return;
		elif logged-out "$page"; then
			page=$(log-in "$page");
			printf "%s " "...";
			continue;
		fi

		# Check for xor.
		if (logged-in "$page" && logged-out "$page") || ! (logged-in "$page" || logged-out "$page"); then
			echo "FAIL";
			echo "Exclusivity check failed.";

			printf "Logged in: ";
			if logged-in "$page"; then
				echo "1";
			else
				echo "0";
			fi

			printf "Logged out: ";
			if logged-out "$page"; then
				echo "1";
			else
				echo "0";
			fi

			echo "We cannot determine whether logged in or out to this service."
			exit 1;
		fi
	done

	if [ "$i" = "0" ]; then
		echo "FAIL";
		echo "Failed to log in to this service.";
		echo "Please check your credentials.";
		exit 2;
	fi
}

assert_pass(){
	if [ -f "$login_file" ]; then
		# Read username and/or password from the login file.
		. "$login_file";
	else
		if [ -z "$username" ] || [ -z "$password" ]; then
			echo "Tip: You can insert your university credentials to '$login_file' to skip this step.";
			echo "Just use these two fields:";
			echo;
			echo "username='u00abc11'";
			echo "password='my-secret-password'";
		fi
	fi

	if [ -z "$username" ]; then
		echo "Username: ";
		read username;
	fi
	if [ -z "$password" ]; then
		echo "Password for $username:";
		read -s password;
	fi
}

assert_service(){
	file="$dir/$1.service";
	if ! [ -f "$file" ]; then
		echo "FAIL";
		echo "Service file does not exist: $file";
		exit 1;
	fi
}

load_service(){
	assert_service "$1";
	. "$dir/$1.service";
}

login_to(){
	printf "%s " "$1";
	load_service "$1";
	page=$(goto-url);
	login-loop "$page" && cache_update "$1";
}

get_all_services(){
	for service in "$dir"/*.service; do
		service=$(basename $service);
		echo ${service%.*};
	done
}

cache_update(){
	service="$1";
	file="$cache/$service";
	date +%s > "$file";
}

cache_ok(){
	# Check if a service cache is still OK.
	refresh_after="$1";
	service="$2";
	# If the cookie jar was somehow removed, it's not OK.
	[ -f "$jar" ] || return;
	assert_service "$service";
	file="$cache/$service";
	# Nonexistent cache file is not OK.
	[ -f "$file" ] || return;
	# We want to refresh every OK from over an hour ago.
	time=$(cat "$file");
	cutoff=$(date --date="$refresh_after" +%s);
	[ "$cutoff" -lt "$time" ] || return;
}
