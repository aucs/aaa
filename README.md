# Aberdeen Authentication Agent

In order to install, you simply need to clone this directory and add `./access` to your $PATH.

Running `access` will refresh your access to all of the services.

You might want to add your credentials to your `~/.abdn.login` for ease of use, provided your home directory is encrypted.
